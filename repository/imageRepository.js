import { createRequire } from "module"; // Bring in the ability to create the 'require' method
const require = createRequire(import.meta.url); // construct the require method
const data = require("../public/data/templates.json")
// const data = require("../public/data/extendedTemplate.json")

import {Image} from '../model/Image.js'

//this layer talks directly with the data layer (i.e. database, data files, etc.)
class ImageRepository {}

ImageRepository.getAll = () => {

    //COMMENTED CODE IS FOR IF ONLY THE ID AND THUMBNAIL ARE REQUIRED
    // const images = []
    // data.forEach((img) => {
    //     images.push({
    //         "id": img.id,
    //         "thumbnail": img.thumbnail
    //     })
    // })
    // return images;

    return data;
}

ImageRepository.getNext = (index, interval) => {
    console.log("hiiiiii", index, interval)
    console.log(data.slice(index, index + interval))
    return data.slice(index, index + interval);
}

ImageRepository.findById = (id) => {
    let image = null;

    data.forEach(img => {
        if(img.id == id) {
            image = img
        }
    })

    return image;
}

export {ImageRepository}