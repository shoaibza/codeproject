import express from 'express'
import { ImageController } from '../controller/ImageController.js'

const router = express.Router();

router.get("/get/all", ImageController.getAll); //get all images
router.get("/getnext/:index/:interval", ImageController.getNext); //get next set of images
router.get("/get/:id", ImageController.findById); //get an image by id

export default router;