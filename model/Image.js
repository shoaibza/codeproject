import mongoose from 'mongoose'
const { Schema, Model } = mongoose

const ImageSchema = new Schema({
    title: {
        type: String
    },
    description: {
        type: String
    },
    cost: {
        type: String
    },
    id: {
        type: String
    },
    thumbnail: {
        type: String
    },
    image: {
        type: String
    }
})

// export default class Image {
//     constructor(title, description, cost, id, thumbnail, image) {
//         this.title = title;
//         this.description = description;
//         this.cost = cost;
//         this.id = id;
//         this.thumbnail = thumbnail;
//         this.image = image;
//     }
// }

const Image = mongoose.model("image", ImageSchema);
export {Image}
