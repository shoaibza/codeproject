import {ImageService} from '../service/ImageService.js';

class ImageController {}

ImageController.findById = (req, res) => {
    const {
        title, description, cost, id, thumbnail, image
    } = req.params //get values from request parameters

    const img = ImageService.findById(id);

    if(img == null) {
        return res.status(400).json({
            "error": "Bad request: Could not find specified image"
        })
    }
    res.status(200).json(img)
}

ImageController.getAll = (req, res) => {
    res.json(ImageService.getAll());
}

ImageController.getNext = (req, res) => {
    const { index, interval } = req.params
    console.log("inside controller", index, interval)
    res.json(ImageService.getNext(index, interval));
}

export { ImageController }