import express from 'express';
import bodyParser from 'body-parser'
import imageRoutes from './routes/ImageRoutes.js'
import cors from 'cors'

const app = express();
const PORT = 5000;

const corsOptions ={
    origin:'*', 
    credentials:true,
    optionSuccessStatus:200,
}
 
app.use(cors(corsOptions))
app.use(bodyParser.json());
app.use("/images", imageRoutes)


app.listen(PORT, () => console.log(`Server runnihng on localhost:${PORT}`))