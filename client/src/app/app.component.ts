import { EventEmitter, Component, Output } from '@angular/core';
import { Image } from './Image'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'codeproject';

  @Output() changeDisplayedFile: EventEmitter<any> = new EventEmitter();
  selectedImage!: Image;

  showFileInfo(img: Image): void {
    console.log("in parent", img)
    this.selectedImage = img;
    this.changeDisplayedFile.emit(img);
  }
}
