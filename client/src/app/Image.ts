export interface Image {
    title: String,
    description: String,
    cost: String,
    id: String,
    thumbnail: String,
    image: String
}