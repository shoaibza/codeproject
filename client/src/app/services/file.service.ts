import { Injectable, Input } from '@angular/core';
import { Image } from '../Image'
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class FileService {
  private apiUrl = "http://localhost:5000/";

  constructor(private http: HttpClient) { }

  // getFiles(): File[] {
  //   return FILES;
  // }

  getAll(): Observable<Image[]> { //get all images
    const url = this.apiUrl + "images/get/all"
    console.log("getting image ids", url)
    return this.http.get<Image[]>(url);
  }

  getNext(index: Number, interval: Number): Observable<Image[]> { //get next set of images
    const url = this.apiUrl + "images/getnext/" + index + "/" + interval
    console.log("getting next images", url)
    return this.http.get<Image[]>(url);
  }

  getById(id: String): Observable<Image> { //get an image by id
    const url = this.apiUrl + "images/get/" + id
    console.log("getting image ids", url)
    return this.http.get<Image>(url);
  }
  
}
