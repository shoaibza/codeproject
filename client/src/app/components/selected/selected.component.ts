import { Component, Input, OnInit } from '@angular/core';
import { Image } from '../../Image'

@Component({
  selector: 'app-selected',
  templateUrl: './selected.component.html',
  styleUrls: ['./selected.component.scss']
})
export class SelectedComponent implements OnInit {

  @Input() image!: Image; //metadata for current selected file

  constructor() { }

  ngOnInit(): void {
    
  }
}
