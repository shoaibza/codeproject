import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FileService } from 'src/app/services/file.service';
import { Image } from '../../Image'

@Component({
  selector: 'app-filmstrip',
  templateUrl: './filmstrip.component.html',
  styleUrls: ['./filmstrip.component.scss']
})
export class FilmstripComponent implements OnInit {

  images!: Image[]; //list of all loaded images metadata
  selectedImage!: Image;

  //to implement slides in the filmstrip component, group images in
  //sets of [visibleImages] which determines how many images displayed at a time
  imageGroups: Image[][] = [];
  currentGroup: Number = 0;
  visibleImages: Number = 4

  @Output() displayFile: EventEmitter<any> = new EventEmitter();

  constructor(private fileService: FileService) { }

  //load images
  ngOnInit(): void {
    this.fileService.getAll().subscribe((data: Image[]) => {
      this.images = data  
      this.selectedImage = data[0]
      
      this.createGroups() //seperate into groupps

      this.displayFile.emit(this.selectedImage)
      
    }), (error: any) => {
      console.log("error from api", error)
    }
  }

  //when an image is clicked
  onClickFile(img: Image) {
    this.selectedImage = img;
    this.displayFile.emit(img)
  }

  createGroups() {
    let i = 0;
    let group: Image[] = [];
    this.images.forEach(image => {
      if(i > this.visibleImages.valueOf() - 1) {
        this.imageGroups.push(group)
        group = []
        i = 0;
      }

      group.push(image)
      i++;
    })
    this.imageGroups.push(group)
    console.log("groups", this.imageGroups)
  }

  prev() {
    console.log("previous 4", this.currentGroup)
    if(this.currentGroup.valueOf() > 0) { //if first group is visible
      this.currentGroup = this.currentGroup.valueOf() - 1
    }
  }

  next() {
    console.log("next 4", this.currentGroup, this.imageGroups.length)
    if(this.currentGroup.valueOf() < this.imageGroups.length - 1) { //if last group is visible
      this.currentGroup = this.currentGroup.valueOf() + 1
    }
  }
}
