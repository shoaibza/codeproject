import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { Image } from '../../Image'
import { SelectedComponent } from '../selected/selected.component';

@Component({
  selector: 'app-image',
  templateUrl: './image.component.html',
  styleUrls: ['./image.component.scss']
})
export class ImageComponent implements OnInit {

  @Input() image!: Image; //metadata for this component
  @Output() select: EventEmitter<any> = new EventEmitter()
  @Input() selected: boolean = false;

  constructor() { }

  ngOnInit(): void {
  }

  selectFile(img: Image): void {
    this.selected = true;
    this.select.emit(img);
  }
}
