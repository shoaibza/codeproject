Installation Instructiions:

1. Make sure to have the latest version of node and npm installed on your machine
2. Go to https://bitbucket.org/shoaibza/codeproject/src/main/ and either clone or download the repository to your local machine
3. Open a terminal and run the command `npm i -g @angular/cli`, this will allow you to run you angular app
4. navigate to the root directory which contains this README file
5. run the command `npm run build`, this will download all required dependancies
6. in order to run both the client and server, the "concurrently" package was used.
7. Simply run the command `npm run dev` and this will execute two scripts, one to run the backend and one for frontend
8. The app should now be up and running!