import {ImageRepository} from '../repository/imageRepository.js'

class ImageService {}

ImageService.findById = (id) => {
    return ImageRepository.findById(id);
}

ImageService.getAll = () => {
    return ImageRepository.getAll();
}

ImageService.getNext = (index, interval) => {
    return ImageRepository.getNext(index, interval);
}

export {ImageService}